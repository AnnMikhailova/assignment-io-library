section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
.loop:
	cmp byte [rdi+rax], 0
	je .end
	inc rax
	jmp .loop

.end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	xor r10, r10
.loop:
	cmp byte [rdi+r10], 0
	je .end
	
	push rdi
	lea rsi, [rdi+r10]
	mov rax, 1
	mov rdi, 1
	mov rdx, 1
	syscall
	pop rdi
	
	inc r10
	jmp .loop
.end:
	ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	mov rsi, rsp
	
	mov rax, 1
	mov rdi, 1
	mov rdx, 1

	syscall
	pop rdi

	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, 0xA
	call print_char
	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	push 0					;string end
	mov rax, rdi			;rax - numb
	mov r10, 10				;divider

.loop:
	xor rdx, rdx
	div r10					;rax/10, rax has remainer, rdx has whole part
	
	add rdx, 48				;rdx+48 - code of last numb
	push rdx
	cmp rax, 0
	je .print				;rax = 0 -> end of number
	jmp .loop	

.print:		
	xor rdi, rdi
	pop rdi
	cmp rdi, 0
	je .end

	call print_char
	
	jmp .print
	
.end:	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	cmp rdi, 0
	jns .pos
	push rdi
	mov rdi, '-'			;'-' sign
	call print_char
	pop rdi
	neg rdi
.pos:
	call print_uint
	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
.loop:
	mov al, byte [rdi]
	mov dl, byte [rsi]
	cmp al, dl
	jne .exit_ne
	
	cmp al, 0
	je .exit

	inc rdi
	inc rsi
	jmp .loop

.exit_ne:
	mov rax, 0
	ret
.exit:
	mov rax, 1
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	mov rax, 0
	mov rdi, 0
	push 0
	mov rsi, rsp
	mov rdx, 1

	syscall
	
	pop rax
.exit:	
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	push rdi
	push rsi
				            ; rdi - cur_addr
	mov r9, rdi		        ; r9 - start_addr
	add rsi, rdi		    ; rsi - end_addr = start+len
	dec rsi			        ; place for 0
.start_space:

	push rdi
	push rsi
	call read_char		    ; symb in rax
	pop rsi
	pop rdi

	cmp rax, 0x20
	je .start_space
	cmp rax, 0x9
	je .start_space
	cmp rax, 0xA
	je .start_space
	cmp rax, 0
	je .err

.letter:
	cmp rdi, rsi
	je .err
	jg .err
	
	mov byte [rdi], al	    ;put symb in buff
	inc rdi

.read_letter:
	push rdi
	push rsi
	call read_char		    ; symb in rax
	pop rsi
	pop rdi
	
	cmp rax, 0x20
	je .no_err
	cmp rax, 0x9
	je .no_err
	cmp rax, 0xA
	je .no_err
	cmp rax, 0
	je .no_err

	jmp .letter

.err:
	mov rax, 0
	pop rsi
	pop rsi
	mov rdx, 0
	jmp .exit

.no_err:
	pop r10
	pop r9
	mov rax, r9
	mov byte[rdi], 0
	sub rdi, r9				; 0_addr - start_addr = word_len
	mov rdx, rdi
	mov rdi, rax
.exit:	
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rcx, rcx		    ;numb_count
	xor r9, r9
	xor rax, rax		    ;numb
	mov r10, 10
.first:
	mov r9b, byte [rdi]	    ;r9 - code_of_numb
	inc rdi			        ;rdi = cur_addr

	cmp r9b, '0'
	jl .non

	cmp r9b, '9'
	jg .non

	inc rcx			
	sub r9b, '0'		    ;r9 - numb
	mov al, r9b		        ;rax - numb

.num:	
    mov r9b, byte [rdi]	    ;r9 - code_of_numb
	inc rdi			        ;rdi = cur_addr
	
	cmp r9b, '0'
	jl .ok

	cmp r9b, '9'
	jg .ok

	sub r9b, '0'		    ;r9 - number

	push rax
	.h: mul r10		        ;rax*10
	jo .ret_state
	add rax, r9		        ;rax*10+r9
	jo .ret_state	
	pop r8

	inc rcx			        ;order_ind
	
	jmp .num

.ret_state:
	pop rax
	jmp .ok
.non:	
	xor rcx, rcx
.ok:
	
	mov rdx, rcx
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:			
	mov r8, 1			    ;sign_mul			
.first:
	mov r9b, byte [rdi]		;r9 - code of symb
					        ;rdi - cur_addr
	cmp r9b, '-'
	je .sign_min
	jmp .num

.sign_min: 
	mov r8, -1
	inc rdi
.num:
	push rdi
	push r8
	call parse_int_numbers	;rax - uint
	pop r8
	pop rdi

	cmp rdx, 0
	je .non

	jmp .ok

.non:
	xor rdx, rdx
	ret
.ok:	
	mov r10, 0			    ;increase rdx for sign
	cmp r8, 0
	jg .no_sign
	mov r10, 1
.no_sign:
	add rdx, r10
	push rdx
	imul r8
	pop rdx
	ret

parse_int_numbers:
	xor rcx, rcx
	xor r9, r9
	xor rax, rax		    ;numb
	mov r10, 10
.first:
	mov r9b, byte [rdi]	    ;r9 - code_of_numb
	inc rdi			        ;rdi = cur_addr

	cmp r9b, '0'
	jl .non

	cmp r9b, '9'
	jg .non

	inc rcx			        ;numb_count
	sub r9b, '0'		    ;r9 - numb
	mov al, r9b		        ;rax - numb

.num:	
    mov r9b, byte [rdi]	    ;r9 - code_of_numb
	inc rdi			        ;rdi = cur_addr
	
	cmp r9b, '0'
	jl .ok

	cmp r9b, '9'
	jg .ok

	sub r9b, '0'		    ;r9 - number

	push rax
	.h: mul r10		        ;rax*10
	jc .ret_state
	add rax, r9		        ;rax*10+r9
	jc .ret_state	
	pop r8

	inc rcx					;order_ind
	
	jmp .num

.ret_state:
	pop rax
	jmp .ok
.non:	
	xor rcx, rcx
.ok:
	mov rdx, rcx
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:			
.check_len: 
	push rdi
	push rsi
	push rdx
	call string_length		;rax - str_len
	pop rdx
	pop rsi
	pop rdi
	
	inc rax
	push rax

	cmp rax, rdx
	jl .copy_symb
	je .copy_symb
	pop rax
	push 0
	mov rax, rdx
	
.copy_symb:	
	cmp rax, 1
	je .ok
	jl .ok

	mov r9b, byte [rdi]
	mov byte [rsi], r9b
	
	dec rax
	inc rdi
	inc rsi

	jmp .copy_symb
.ok:
	
	mov byte [rsi], 0
	pop rax
.end:
	ret
